package org.misha;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

// The compareAndSet() method of an AtomicReference class is used to atomically update the value
// of the AtomicReference object to a newValue and to get, as a return, the fact that operation has been successful.
// The value is updated if and only if the current value of AtomicReference object equals to the expectedValue.
// The method returns true if operation is successful else returns false.
//
// public final boolean compareAndSet(V expectedValue, V newValue)

public class LockFreeLinkedList<T> implements Iterable<T>, Collection<T> {
    private final AtomicInteger size;
    private final AtomicReference<Node<T>> head, tail;

    public LockFreeLinkedList() {
        head = new AtomicReference<>(null);
        tail = new AtomicReference<>(null);
        size = new AtomicInteger();
    }

    public int size() {
        return size.get();
    }

    public void push(T value) {
        if (value == null) {
            throw new NullPointerException();
        }
        Node<T> newNode = new Node<>(value);
        Node<T> currentTail;
        do {
            currentTail = tail.get();
            newNode.prev = currentTail;
        } while (!tail.compareAndSet(currentTail, newNode));
        if (newNode.prev != null) {
            newNode.prev.next = newNode;
        }
        head.compareAndSet(null, newNode); // for inserting the first element
    }

    public T pop() {
        if (head.get() == null) {
            return null;
        }
        Node<T> currentHead;
        Node<T> nextNode;
        do {
            currentHead = head.get();
            nextNode = currentHead.next;
        } while (!head.compareAndSet(currentHead, nextNode));
        size.decrementAndGet();
        return currentHead.value;
    }

    public boolean isEmpty() {
        return head.get() == null;
    }

    public boolean contains(Object o) {
        return false;
    }

    public static class Node<T> {
        private final T value;
        private volatile Node<T> next;
        private volatile Node<T> prev;

        private Node(T value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new It<>(head.get());
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[size.get()];
        int i = 0;
        for (LockFreeLinkedList.Node<T> x = head.get(); x != null; x = x.next)
            result[i++] = x.value;
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <S> S[] toArray(S[] a) {
        if (a.length < slowSize())
            a = (S[])java.lang.reflect.Array.newInstance(
                    a.getClass().getComponentType(), size.get());
        int i = 0;
        Object[] result = a;
        for (Node<T> x = head.get(); x != null; x = x.next)
            result[i++] = x.value;

        if (a.length > slowSize())
            a[slowSize()] = null;

        return a;
    }

    @Override
    public boolean add(T t) {
        push(t);
        return  true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }


    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
    }

    @Override
    public Spliterator<T> spliterator() {
        return new LSSpliterator<>( this, slowSize());
    }

    public static final class It<T> implements Iterator<T> {
        private Node<T> node;
        private final Node<T> root;

        private It(Node<T> root) {
            this.root = root;
            this.node = null;
        }

        @Override
        public boolean hasNext() {
            return node == null || node.next != null;
        }

        @Override
        public T next() {
            node = node == null ? root : node.next;
            return node.value;
        }
    }

    @Override
    synchronized public String toString() {
        StringBuilder sb = new StringBuilder();
        for (T t : this) {
            sb.append(t).append(' ');
        }
        return sb.toString();
    }

    synchronized int slowSize() {
        int result = 0;
        Node<T> node = head.get();
        do {
            if (node == null) continue;
            ++result;
            node = node.next;
        } while (node != null);
        return result;
    }

    static final class LSSpliterator<E> implements Spliterator<E> {
        static final int BATCH_UNIT = 1 << 10;  // batch array size increment
        static final int MAX_BATCH = 1 << 25;  // max batch array size;
        final LockFreeLinkedList<E> list; // null OK unless traversed
        Node<E> current;      // current node; null until initialized
        int est;              // size estimate; -1 until first needed
        int batch;            // batch size for splits

        LSSpliterator(LockFreeLinkedList<E> list, int est) {
            this.list = list;
            this.est = est;
            current = list.head.get();
        }

        final int getEst() {
            int s; // force initialization
            if ((s = est) < 0) {
                if (list == null)
                    s = est = 0;
            }
            return s;
        }

        public long estimateSize() {
            return getEst();
        }

        public Spliterator<E> trySplit() {
            Node<E> p;
            int s = getEst();
            if (s > 1 && (p = current) != null) {
                int n = batch + BATCH_UNIT;
                if (n > s)
                    n = s;
                if (n > MAX_BATCH)
                    n = MAX_BATCH;
                Object[] a = new Object[n];
                int j = 0;
                do {
                    a[j++] = p.value;
                } while ((p = p.next) != null && j < n);
                current = p;
                batch = j;
                est = s - j;
                return Spliterators.spliterator(a, 0, j, Spliterator.ORDERED);
            }
            return null;
        }

        public void forEachRemaining(Consumer<? super E> action) {
            Node<E> p; int n;
            if (action == null) throw new NullPointerException();
            if ((n = getEst()) > 0 && (p = current) != null) {
                current = null;
                est = 0;
                do {
                    E e = p.value;
                    p = p.next;
                    action.accept(e);
                } while (p != null && --n > 0);
            }
        }

        public boolean tryAdvance(Consumer<? super E> action) {
            Node<E> p;
            if (action == null) throw new NullPointerException();
            if (getEst() > 0 && (p = current) != null) {
                --est;
                E e = p.value;
                current = p.next;
                action.accept(e);
                return true;
            }
            return false;
        }

        public int characteristics() {
            return Spliterator.ORDERED | Spliterator.SIZED | Spliterator.SUBSIZED;
        }
    }
}
