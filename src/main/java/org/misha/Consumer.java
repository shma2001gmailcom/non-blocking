package org.misha;

import java.util.concurrent.Callable;

public interface Consumer<T> extends Callable<Void> {

    @SuppressWarnings("all")
    T doConsume();
}
