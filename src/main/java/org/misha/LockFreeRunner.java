package org.misha;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.concurrent.Executors.newWorkStealingPool;

@Slf4j
public class LockFreeRunner {
    private final ExecutorService pool;
    @Getter
    private final LockFreeLinkedList<Integer> list;
    private final Callable<Void> consumer;
    private final Callable<Void> producer;
    private final int producersCount;
    private final int consumersCount;
    private final CountDownLatch latch;

    public LockFreeRunner(int producersCount, int consumersCount, int totalProduce, int bufferLimit) {
        this.producersCount = producersCount;
        this.consumersCount = consumersCount;
        pool = newWorkStealingPool();
        list = new LockFreeLinkedList<>();
        latch = new CountDownLatch(2);
        consumer = new ConsumerTask(new AtomicInteger(), totalProduce, list, latch);
        producer = new ProducerTask(new AtomicInteger(), totalProduce, new AtomicBoolean(), list, bufferLimit, latch);
    }

    public void run()  {
        try {
            pool.invokeAll(Stream.concat(
                    IntStream.range(0, consumersCount).mapToObj(i -> consumer),
                    IntStream.range(0, producersCount).mapToObj(i -> producer)).toList());
            latch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return list.toString();
    }
}
