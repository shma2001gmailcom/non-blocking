package org.misha;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

@Slf4j
public class ConsumerTask implements Callable<Void>, Consumer<Integer> {
    private final AtomicInteger consumedCount;
    private final LockFreeLinkedList<Integer> list;
    private final Predicate<Integer> stopSignal;
    private final CountDownLatch latch;

    public ConsumerTask(AtomicInteger consumedCount,
                        int totalProduce,
                        LockFreeLinkedList<Integer> list,
                        CountDownLatch latch
    ) {
        this.consumedCount = consumedCount;
        this.list = list;
        stopSignal = totalProduce > 0 ? (count) -> count < totalProduce : (count) -> true;
        this.latch = latch;
    }

    @Override
    public Void call() {
        try {
            while (stopSignal.test(consumedCount.get())) {
                doConsume();
            }
            latch.countDown();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        while (!list.isEmpty()) {
            doConsume();
        }
        return null;
    }

    @Override
    public Integer doConsume() {
        Integer pop = list.pop();
        log.info("c {}", list.slowSize());
        if (pop != null) {
            consumedCount.incrementAndGet();
            return pop;
        }
        return null;
    }
}

