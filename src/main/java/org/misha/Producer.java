package org.misha;

public interface Producer {

    void doProduce();
}
