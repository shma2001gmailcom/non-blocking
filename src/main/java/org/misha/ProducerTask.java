package org.misha;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

@Slf4j
public class ProducerTask implements Callable<Void>, Producer {
    private final AtomicInteger producedCount;
    private final AtomicBoolean full;
    private final LockFreeLinkedList<Integer> list;
    private final int bufferLimit;
    private final Predicate<Integer> stopSignal;
    private final CountDownLatch latch;

    public ProducerTask(AtomicInteger producedCount,
                        int totalProduce,
                        AtomicBoolean isFull,
                        LockFreeLinkedList<Integer> list,
                        int bufferLimit,
                        CountDownLatch latch) {
        this.producedCount = producedCount;
        this.full = isFull;
        this.list = list;
        this.bufferLimit = bufferLimit;
        stopSignal = totalProduce > 0 ? (count) -> count > totalProduce : (count) -> false;
        this.latch = latch;
    }

    @Override
    public Void call() {
        try {
            try {
                while (!stopSignal.test(producedCount.get())) {
                    doProduce();
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return null;
        } finally {
            latch.countDown();
        }
    }

    @Override
    public void doProduce() {
        if (!full.compareAndSet(list.size() < bufferLimit, true)) { // non-blocking test
            int slowSize = list.slowSize();
            synchronized (this) {
                if (slowSize < bufferLimit) { // blocking test
                    list.push(producedCount.getAndIncrement());
                    log.info("   p {}", slowSize);
                }
            }
        }
    }
}


