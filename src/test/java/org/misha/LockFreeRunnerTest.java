package org.misha;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
class LockFreeRunnerTest {

    @Test
    void run() {
        LockFreeRunner lockFreeRunner = new LockFreeRunner(3, 4, 100_000, 2);
        lockFreeRunner.run();
        assertTrue(lockFreeRunner.getList().isEmpty());
    }
}